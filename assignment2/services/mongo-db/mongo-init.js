db.auth('admin', 'admin')

db = db.getSiblingDB('beverages')

db.createUser({
  user: 'tester',
  pwd: 'tester',
  roles: [
    {
      role: 'root',
      db: 'admin',
    },
  ],
});