package de.uniba.dsg.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import de.uniba.dsg.jaxrs.delete.DeleteBottle;
import de.uniba.dsg.jaxrs.delete.DeleteCrate;
import de.uniba.dsg.jaxrs.get.*;
import de.uniba.dsg.jaxrs.post.CreateBottle;
import de.uniba.dsg.jaxrs.post.CreateCrate;
import de.uniba.dsg.jaxrs.put.UpdateBottle;
import de.uniba.dsg.jaxrs.put.UpdateCrate;

@ApplicationPath("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DBHandlerApi extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(GetAllBeverages.class);
        resources.add(GetAllBottles.class);
        resources.add(GetAllCrates.class);
        resources.add(GetBottle.class);
        resources.add(GetCrate.class);
        resources.add(CreateBottle.class);
        resources.add(CreateCrate.class);
        resources.add(UpdateBottle.class);
        resources.add(UpdateCrate.class);
        resources.add(DeleteBottle.class);
        resources.add(DeleteCrate.class);

        return resources;
    }
}
