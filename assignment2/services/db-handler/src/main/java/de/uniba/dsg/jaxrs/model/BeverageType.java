package de.uniba.dsg.jaxrs.model;

public enum BeverageType {
    BOTTLE,CRATE;
}
