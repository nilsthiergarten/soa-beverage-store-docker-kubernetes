package de.uniba.dsg.jaxrs.model.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "beverageList")
@XmlType(propOrder = { "beverages"})
public class BeverageListDTO {
    private List<BeverageDTO> beverages;

    public BeverageListDTO(List<BeverageDTO> beverages) {
        this.beverages = beverages;
    }
    public BeverageListDTO() {
    }
}
