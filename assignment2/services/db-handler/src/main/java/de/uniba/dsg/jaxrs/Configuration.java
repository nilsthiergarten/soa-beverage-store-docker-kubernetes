package de.uniba.dsg.jaxrs;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

public class Configuration
{
    private static final Logger LOGGER = Logger.getLogger(Configuration.class.getName());

    public static Properties loadProperties()
    {
        try (InputStream stream = DBHandlerApi.class.getClassLoader().getResourceAsStream("config.properties")) {
            Properties properties = new Properties();
            properties.load(stream);

            Set<String> keys = properties.stringPropertyNames();
            for (String key : keys) {
                Optional<String> envVar = Optional.ofNullable(System.getenv(key.toUpperCase()));
                envVar.ifPresent(newValue -> properties.setProperty(key, newValue));
            }

            return properties;
        }
        catch (IOException e) {
            LOGGER.severe("Cannot load configuration file.");
            return null;
        }
    }
}
