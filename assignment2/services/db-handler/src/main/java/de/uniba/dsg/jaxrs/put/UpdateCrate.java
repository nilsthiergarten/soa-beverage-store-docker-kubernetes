package de.uniba.dsg.jaxrs.put;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.logging.Logger;

@Path("crates")
public class UpdateCrate {
    private static final Logger LOGGER = Logger.getLogger(UpdateCrate.class.getName());

    @PUT
    @Path("{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCrate(@Context final UriInfo info, @PathParam("crateId") final int id, final CrateDTO updatedCrate) {
        LOGGER.info("Update Crate with id " + id + ". " + updatedCrate);
        if (updatedCrate == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage(ErrorType.INVALID_PARAMETER, "Body was empty")).build();
        }

        final Crate crate = DBClient.getInstance().getCrateById(id);

        if (crate == null) {
            LOGGER.warning(
                    String.format("Crate with id %d could not be deleted because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbPUT()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorType(ErrorType.RESOURCE_UNMODIFIABLE)
                    .errorMessage(String.format("Crate with id %d not found", id)).build();
        }

        // set dummy bottle if bottle is null
        if(updatedCrate.getBottle() == null){
            updatedCrate.setBottle(new BottleDTO());
        }

        final Crate resultCrate = DBClient.getInstance().updateCrate(id, updatedCrate.convert());

        LOGGER.info("Crate with id " + resultCrate.getId() + " successfully updated.");
        return Response.ok().build();
    }
}
