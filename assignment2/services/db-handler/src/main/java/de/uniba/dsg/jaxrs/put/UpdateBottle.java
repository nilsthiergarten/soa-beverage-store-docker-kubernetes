package de.uniba.dsg.jaxrs.put;

import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;
import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.Bottle;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Optional;
import java.util.logging.Logger;

@Path("bottles")
public class UpdateBottle {
    private static final Logger LOGGER = Logger.getLogger(UpdateBottle.class.getName());

    @PUT
    @Path("{bottleId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBottle(@Context final UriInfo info, @PathParam("bottleId") final int id, final BottleDTO updatedBottle) {
        LOGGER.info("Update Bottle with id " + id + ". " + updatedBottle);
        if (updatedBottle == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage(ErrorType.INVALID_PARAMETER, "Body was empty")).build();
        }

        final Optional<Bottle> optionalBottle = DBClient.getInstance().getBottleById(id);

        if (optionalBottle.isPresent()) {
            final Bottle resultBottle = DBClient.getInstance().updateBottle(id, updatedBottle.convert());
            LOGGER.info("Bottle with id " + resultBottle.getId() + " and name " + resultBottle.getName() + " successfully updated.");
            return Response.ok().build();
        }else {
            LOGGER.warning(
                    String.format("Bottle with id %d could not be modified because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbPUT()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorType(ErrorType.RESOURCE_UNMODIFIABLE)
                    .errorMessage(String.format("Bottle with id %d not found", id)).build();
        }
    }
}
