package de.uniba.dsg.jaxrs.model.api;

import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.net.URI;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"pagination", "beverageDTOS", "href"})
public class PaginatedBeverages {

    private Pagination pagination;
    private List<BeverageDTO> beverageDTOS;
    private URI href;

    public PaginatedBeverages(Pagination pagination, List<BeverageDTO> beverageDTOS, URI href) {
        this.pagination = pagination;
        this.beverageDTOS = beverageDTOS;
        this.href = href;
    }

    public PaginatedBeverages() {
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<BeverageDTO> getBeverageDTOS() {
        return beverageDTOS;
    }

    public void setBeverageDTOS(List<BeverageDTO> beverageDTOS) {
        this.beverageDTOS = beverageDTOS;
    }

    public URI getHref() {
        return href;
    }

    public void setHref(URI href) {
        this.href = href;
    }
}
