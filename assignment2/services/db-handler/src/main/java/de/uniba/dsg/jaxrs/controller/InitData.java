package de.uniba.dsg.jaxrs.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;

/**
 * Placeholder for default database initializiation
 * Use carefully!
 */

public class InitData {

    public final List<BottleDTO> bottles;
    public final List<CrateDTO> crates;

    public InitData() {
        this.bottles = initBottles();
        this.crates = initCases();

    }

    private List<BottleDTO> initBottles() {
        return new ArrayList<>(Arrays.asList(
             new BottleDTO(  new Bottle(1, "Pils", 0.5, true, 4.8, 0.79, "Keesmann", 34)),
             new BottleDTO(  new Bottle(2, "Helles", 0.5, true, 4.9, 0.89, "Mahr", 17)),
             new BottleDTO(  new Bottle(3, "Boxbeutel", 0.75, true, 12.5, 5.79, "Divino", 11)),
             new BottleDTO(  new Bottle(4, "Tequila", 0.7, true, 40.0, 13.79, "Tequila Inc.", 5)),
             new BottleDTO(  new Bottle(5, "Gin", 0.5, true, 42.00, 11.79, "Hopfengarten", 3)),
             new BottleDTO(  new Bottle(6, "Export Edel", 0.5, true, 4.8, 0.59, "Oettinger", 66)),
             new BottleDTO(  new Bottle(7, "Premium Tafelwasser", 0.7, false, 0.0, 4.29, "Franken Brunnen", 12)),
             new BottleDTO(  new Bottle(8, "Wasser", 0.5, false, 0.0, 0.29, "Franken Brunnen", 57)),
             new BottleDTO(  new Bottle(9, "Spezi", 0.7, false, 0.0, 0.69, "Franken Brunnen", 42)),
             new BottleDTO(  new Bottle(10, "Grape Mix", 0.5, false, 0.0, 0.59, "Franken Brunnen", 12)),
             new BottleDTO(  new Bottle(11, "Still", 1.0, false, 0.0, 0.66, "Franken Brunnen", 34)),
             new BottleDTO(  new Bottle(12, "Cola", 1.5, false, 0.0, 1.79, "CCC", 69)),
             new BottleDTO(  new Bottle(13, "Cola Zero", 2.0, false, 0.0, 2.19, "CCC", 12)),
             new BottleDTO(  new Bottle(14, "Apple", 0.5, false, 0.0, 1.99, "Juice Factory", 25)),
             new BottleDTO(  new Bottle(15, "Orange", 0.5, false, 0.0, 1.99, "Juice Factory", 55)),
             new BottleDTO(  new Bottle(16, "Lime", 0.5, false, 0.0, 2.99, "Juice Factory", 8)
        )));
    }

    private List<CrateDTO> initCases() {
        return new ArrayList<>(Arrays.asList(
                new CrateDTO(new Crate(1, this.bottles.get(0).convert(), 20, 14.99, 3)),
                new CrateDTO(new Crate(2, this.bottles.get(1).convert(), 20, 15.99, 5)),
                new CrateDTO(new Crate(3, this.bottles.get(2).convert(), 6, 30.00, 7)),
                new CrateDTO(new Crate(4, this.bottles.get(7).convert(), 12, 1.99, 11)),
                new CrateDTO(new Crate(5, this.bottles.get(8).convert(), 20, 11.99, 13)),
                new CrateDTO(new Crate(6, this.bottles.get(11).convert(), 6, 10.99, 4)),
                new CrateDTO(new Crate(7, this.bottles.get(12).convert(), 6, 11.99, 5)),
                new CrateDTO(new Crate(8, this.bottles.get(13).convert(), 20, 35.00, 7)),
                new CrateDTO(new Crate(9, this.bottles.get(14).convert(), 12, 20.00, 9)
        )));
    }

}
