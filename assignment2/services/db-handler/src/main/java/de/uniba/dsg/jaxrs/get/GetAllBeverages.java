package de.uniba.dsg.jaxrs.get;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;

@Path("beverages")
public class GetAllBeverages {

    private static final Logger LOGGER = Logger.getLogger(GetAllBeverages.class.getName());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBeverage() {
        LOGGER.info("Get all beverages");
        List<Beverage> beverages = DBClient.getInstance().getBeverages();
        LOGGER.info("Found "+beverages.size()+" Beverages ");
        BeverageListDTO beverageListDTO = new BeverageListDTO(BeverageDTO.marshallList(beverages));

        return Response.ok(beverageListDTO).build();
    }
}

