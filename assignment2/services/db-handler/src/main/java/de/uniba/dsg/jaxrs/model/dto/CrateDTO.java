package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;

import javax.xml.bind.annotation.*;
import java.net.URI;

import org.bson.types.ObjectId;

public class CrateDTO {
    public ObjectId id;

    public int identifier;

    public double price;

    public int inStock;

    public BottleDTO bottle;

    public int noOfBottles;


    public CrateDTO() {
    }

    public CrateDTO(int id, BottleDTO bottle, int noOfBottles, double price, int inStock) {
        this.identifier = id;
        this.bottle = bottle;
        this.noOfBottles = noOfBottles;
        this.price = price;
        this.inStock = inStock;

    }

    public CrateDTO(final Crate crate) {
        this.identifier = crate.getId();
        this.bottle = new BottleDTO(crate.getBottle());
        this.noOfBottles = crate.getNoOfBottles();
        this.price = crate.getPrice();
        this.inStock = crate.getInStock();
    }


    public Crate convert() {
        return new Crate(this.identifier, this.bottle.unmarshall(), this.noOfBottles, this.price, this.inStock);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public BottleDTO getBottle() {
        return bottle;
    }

    public void setBottle(BottleDTO bottle) {
        this.bottle = bottle;
    }

    public int getNoOfBottles() {
        return noOfBottles;
    }

    public void setNoOfBottles(int noOfBottles) {
        this.noOfBottles = noOfBottles;
    }

    @Override
    public String toString() {
        return "CrateDTO [" +
                "id=" + id +
                ", identifier=" + identifier +
                ", price=" + price +
                ", inStock=" + inStock +
                ", bottle=" + bottle +
                ", noOfBottles=" + noOfBottles +
                ']';
    }
}
