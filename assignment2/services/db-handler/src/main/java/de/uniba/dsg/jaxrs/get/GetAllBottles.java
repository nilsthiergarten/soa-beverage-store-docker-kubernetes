package de.uniba.dsg.jaxrs.get;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("beverages")
public class GetAllBottles {

    private static final Logger LOGGER = Logger.getLogger(GetAllBottles.class.getName());

    @GET
    @Path("bottles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllBottles() {
        LOGGER.info("Get all bottles.");
        List<Beverage> bottles = DBClient.getInstance().getBottles();
        LOGGER.info("Found " + bottles.size() + " Bottles ");
        BeverageListDTO beverageListDTO = new BeverageListDTO(BeverageDTO.marshallList(bottles));

        return Response.ok(beverageListDTO).build();
    }
}
