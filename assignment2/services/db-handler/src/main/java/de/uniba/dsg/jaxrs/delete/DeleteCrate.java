package de.uniba.dsg.jaxrs.delete;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

import javax.validation.constraints.Min;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.logging.Logger;

@Path("crates")
public class DeleteCrate {
    private static final Logger LOGGER = Logger.getLogger(DeleteCrate.class.getName());

    @DELETE
    @Path("{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCrate(
            @Context final UriInfo info,
            @PathParam("crateId") @Min(value = 1) final int id) {
        LOGGER.info("Delete Crate with id " + id);

        // Delete Crate in database
        final boolean deleted = DBClient.getInstance().deleteCrate(id);

        if (!deleted) {
            LOGGER.warning(
                    String.format("Crate with id %d could not be deleted because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbDELETE()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorType(ErrorType.RESOURCE_UNDELETABLE)
                    .errorMessage(String.format("Crate with id %d not found", id)).build();
        } else {
            LOGGER.info("Crate with id " + id + " successfully deleted.");
            return Response.ok().build();
        }
    }
}