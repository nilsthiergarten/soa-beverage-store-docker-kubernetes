package de.uniba.dsg.jaxrs.delete;

import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;
import de.uniba.dsg.jaxrs.controller.DBClient;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.logging.Logger;

@Path("bottles")
public class DeleteBottle {
    private static final Logger LOGGER = Logger.getLogger(DeleteBottle.class.getName());

    @DELETE
    @Path("{bottleId}")
    public Response deleteBottle(
            @Context final UriInfo info,
            @PathParam("bottleId") final int id) {
        LOGGER.info("Delete Bottle with id " + id);
        final boolean deleted = DBClient.getInstance().deleteBottleById(id);

        if (!deleted) {
            LOGGER.warning(
                    String.format("Bottle with id %d could not be deleted because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbDELETE()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorType(ErrorType.RESOURCE_UNDELETABLE)
                    .errorMessage(String.format("Bottle with id %d not found", id)).build();
        } else {
            LOGGER.info("Bottle with id " + id + " successfully deleted.");
            return Response.ok().build();
        }
    }
}
