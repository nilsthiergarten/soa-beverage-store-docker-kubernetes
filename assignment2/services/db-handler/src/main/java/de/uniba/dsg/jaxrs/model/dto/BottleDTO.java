package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Bottle;

import javax.xml.bind.annotation.*;
import java.net.URI;

import org.bson.types.ObjectId;

public class BottleDTO {

    public ObjectId id;

    public int identifier;

    public String name;

    public double price;

    public int inStock;

    public double volume;

    public boolean isAlcoholic;

    public double volumePercent;
    public String supplier;




    public Bottle unmarshall() {
        return new Bottle(this.identifier, this.name, this.volume, this.isAlcoholic, this.volumePercent, this.price,
                this.supplier, this.inStock);
    }

    public BottleDTO(int identifier, String name, double price, int inStock, double volume, boolean isAlcoholic,
            double volumePercent, String supplier) {
        this.identifier = identifier;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.volume = volume;
        this.isAlcoholic = isAlcoholic;
        this.volumePercent = volumePercent;
        this.supplier = supplier;
    }

    public BottleDTO (Bottle bottle){
        this.identifier = bottle.getId();
        this.name = bottle.getName();
        this.price = bottle.getPrice();
        this.inStock = bottle.getInStock();
        this.volume = bottle.getVolume();
        this.isAlcoholic = bottle.isAlcoholic();
        this.volumePercent = bottle.getVolumePercent();
        this.supplier = bottle.getSupplier();
    }

    public Bottle convert() {
        return new Bottle(identifier,name,volume,isAlcoholic,volumePercent,price,supplier,inStock);
    }

    public BottleDTO() {
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public boolean isAlcoholic() {
        return isAlcoholic;
    }

    public void setAlcoholic(boolean alcoholic) {
        isAlcoholic = alcoholic;
    }

    public double getVolumePercent() {
        return volumePercent;
    }

    public void setVolumePercent(double volumePercent) {
        this.volumePercent = volumePercent;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return "BottleDTO [" +
                "id=" + id +
                ", identifier=" + identifier +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", inStock=" + inStock +
                ", volume=" + volume +
                ", isAlcoholic=" + isAlcoholic +
                ", volumePercent=" + volumePercent +
                ", supplier='" + supplier + '\'' +
                ']';
    }
}
