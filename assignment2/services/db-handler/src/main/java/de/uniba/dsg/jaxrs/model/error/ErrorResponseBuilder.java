package de.uniba.dsg.jaxrs.model.error;

import java.net.URI;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

public final class ErrorResponseBuilder
{
    private Status status = Status.INTERNAL_SERVER_ERROR;
    private ErrorType errorType = ErrorType.UNDEFINED;
    private String errorMessage = "Error";
    private String path = "undefined";
    private String verb = "undefined";
    private String bodyType = "application/json";

    private ErrorResponseBuilder()
    {
    }

    /**
     * 400 Bad Request, see {@link <a href=
     * "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1">HTTP/1.1
     * documentation</a>}.
     */
    public static ErrorResponseBuilder badRequest()
    {
        ErrorResponseBuilder builder = new ErrorResponseBuilder();
        builder.status = Status.BAD_REQUEST;
        return builder;
    }

    /**
     * 404 Not Found, see {@link <a href=
     * "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5">HTTP/1.1
     * documentation</a>}.
     */
    public static ErrorResponseBuilder notFound()
    {
        ErrorResponseBuilder builder = new ErrorResponseBuilder();
        builder.status = Status.NOT_FOUND;
        return builder;
    }

    /**
     * 304 Not Modified, see {@link <a href=
     * "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.3.5">HTTP/1.1
     * documentation</a>}.
     */
    public static ErrorResponseBuilder notModified()
    {
        ErrorResponseBuilder builder = new ErrorResponseBuilder();
        builder.status = Status.NOT_MODIFIED;
        return builder;
    }

    /**
     * 500 Internal Server Error, see {@link <a href=
     * "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.1">HTTP/1.1
     * documentation</a>}.
     */
    public static ErrorResponseBuilder internalServerError()
    {
        ErrorResponseBuilder builder = new ErrorResponseBuilder();
        builder.status = Status.INTERNAL_SERVER_ERROR;
        return builder;
    }

    /**
     * 403 Forbidden, see {@link <a href=
     * "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4">HTTP/1.1
     * documentation</a>}.
     */
    public static ErrorResponseBuilder forbidden()
    {
        ErrorResponseBuilder builder = new ErrorResponseBuilder();
        builder.status = Status.FORBIDDEN;
        return builder;
    }

    public ErrorResponseBuilder errorType(ErrorType errorType)
    {
        this.errorType = errorType;
        return this;
    }

    public ErrorResponseBuilder errorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
        return this;
    }

    public ErrorResponseBuilder uriInfo(UriInfo uriInfo)
    {
        this.path = uriInfo.getRequestUri().toString();
        return this;
    }

    public ErrorResponseBuilder uriInfo(URI uri)
    {
        this.path = uri.toString();
        return this;
    }

    public ErrorResponseBuilder verbGET()
    {
        this.verb = "GET";
        return this;
    }

    public ErrorResponseBuilder verbPOST()
    {
        this.verb = "POST";
        return this;
    }

    public ErrorResponseBuilder verbPUT()
    {
        this.verb = "PUT";
        return this;
    }

    public ErrorResponseBuilder verbDELETE()
    {
        this.verb = "DELETE";
        return this;
    }

    public ErrorResponseBuilder verb(String verb)
    {
        this.verb = verb;
        return this;
    }

    public ErrorResponseBuilder bodyType(String bodyType)
    {
        this.bodyType = bodyType;
        return this;
    }

    public Response build()
    {
        ErrorResponse response = new ErrorResponse(new ErrorMessage(errorType, errorMessage), path, verb);
        return Response.status(status).entity(response).type(bodyType).build();
    }
}
