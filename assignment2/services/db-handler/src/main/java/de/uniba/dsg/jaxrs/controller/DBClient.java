package de.uniba.dsg.jaxrs.controller;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.uniba.dsg.jaxrs.Configuration;
import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class DBClient {
	private static final Logger LOGGER = Logger.getLogger(DBClient.class.getName());
	private final static Object lock = new Object();

	private MongoCollection<BottleDTO> bottleCollection;
	private MongoCollection<CrateDTO> crateCollection;

	private static final Semaphore sem = new Semaphore(1);
	private static DBClient instance;

	private DBClient() {
		final Properties props = Configuration.loadProperties();
		final MongoCredential credentials = MongoCredential.createCredential(props.getProperty("DB_USER"), props.getProperty("DB_USER_TABLE"), props.getProperty("DB_PWD").toCharArray());
		final MongoClientOptions options = MongoClientOptions.builder().sslEnabled(false).build();
		final MongoClient client = new MongoClient(new ServerAddress(props.getProperty("SERVICE_NAME"), Integer.parseInt(props.getProperty("MONGO_DB_PORT"))), credentials, options);

		final CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));

		final MongoDatabase database = client.getDatabase(props.getProperty("MONGO_DB_TABLE")).withCodecRegistry(pojoCodecRegistry);

		boolean alreadyInitialized  = database.listCollectionNames().into(new ArrayList<String>()).contains("Bottles");
		LOGGER.info("Database is already initialized " + alreadyInitialized);

		bottleCollection = database.getCollection("Bottles", BottleDTO.class);
		crateCollection = database.getCollection("Crates", CrateDTO.class);
		if(!alreadyInitialized){
			LOGGER.info("Initialize Database with Demodata!");
			setupDB();
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				client.close();
			}
		});
	}

	// =============
	// == DB ==
	// =============

	public void setupDB(){
		InitData db = new InitData();

		bottleCollection.insertMany(db.bottles);
		crateCollection.insertMany(db.crates);
	}

	public static DBClient getInstance() {
		try {
			sem.acquireUninterruptibly();
			if (DBClient.instance == null) {
				DBClient.instance = new DBClient();
			}
		} finally {
			sem.release();
		}
		return DBClient.instance;
	}

	// =============
	// == Beverages ==
	// =============

	public List<Beverage> getBeverages(){
		synchronized (lock) {
			List<Beverage> result = new ArrayList<>();
			result.addAll(getBottles());
			result.addAll(getCrates());
			return result;
		}
	}

	// =============
	// == Bottles ==
	// =============

	private List<Bottle> convertBottle(FindIterable<BottleDTO> iterableBottles) {
		synchronized (lock) {
			List<Bottle> bottles = new ArrayList<>();
			Consumer<BottleDTO> consumer = b -> bottles.add(b.convert());
			iterableBottles.forEach(consumer);
			return bottles;
		}
	}

	public List<Beverage> getBottles(){
		synchronized (lock) {
			List<Bottle> bottles = this.convertBottle(bottleCollection.find());

			List<Beverage> result = new ArrayList<>();
			result.addAll(bottles);
			return result;
		}
	}

	public Optional<Bottle> getBottleById(int id) {
		synchronized (lock) {
			List<Bottle> bottles = this.convertBottle(bottleCollection.find(eq("identifier", id)));
			if (bottles.size() == 0) {
				return Optional.empty();
			}
			return Optional.ofNullable(bottles.get(0));
		}
	}

	public Bottle getBottleByName(String name) {
		synchronized (lock) {
			List<Bottle> bottles = this.convertBottle(bottleCollection.find(eq("name", name)));
			if (bottles.size() == 0) {
				return null;
			}
			return bottles.get(0);
		}
	}

	public boolean deleteBottleById(int id) {
		synchronized (lock) {
			long deleted = bottleCollection.deleteOne(eq("identifier", id)).getDeletedCount();
			if (deleted == 1) {
				return true;
			} else {
				return false;
			}
		}
	}

	public Bottle addBottle(Bottle newBottle) {
		synchronized (lock) {
			bottleCollection.insertOne(new BottleDTO(newBottle));
			Optional<Bottle> optionalBottle = this.getBottleById(newBottle.getId());
			return optionalBottle.get();
		}
	}

	public Bottle updateBottle(int id, Bottle updatedBottle) {
		synchronized (lock) {
			Document document = new Document();
			Optional.ofNullable(updatedBottle.getName()).ifPresent(d -> document.append("name", d));
			Optional.of(updatedBottle.getPrice()).ifPresent(d -> document.append("price", d));
			Optional.of(updatedBottle.getInStock()).ifPresent(d -> document.append("inStock", d));
			Optional.ofNullable(updatedBottle.getSupplier()).ifPresent(d -> document.append("supplier", d));
			Optional.ofNullable(updatedBottle.isAlcoholic()).ifPresent(d -> document.append("isAlcoholic", d));

			bottleCollection.updateOne(eq("identifier", id), new Document("$set", document));
			LOGGER.info("Updated Bottle with" + id);

			Optional<Bottle> optionalBottle = this.getBottleById(id);

			return optionalBottle.get();
		}
	}

	// =============
	// == Crates ==
	// =============

	private List<Crate> convertCrate(FindIterable<CrateDTO> iterableCrates) {
		synchronized (lock) {
			List<Crate> crates = new ArrayList<>();
			Consumer<CrateDTO> consumer = c -> crates.add(c.convert());
			iterableCrates.forEach(consumer);
			return crates;
		}
	}

	public List<Beverage> getCrates(){
		synchronized (lock) {
			List<Crate> crates = this.convertCrate(crateCollection.find());

			List<Beverage> result = new ArrayList<>();
			result.addAll(crates);
			return result;
		}
	}

	public Crate getCrateById(int id) {
		synchronized (lock) {
			List<Crate> crates = this.convertCrate(crateCollection.find(eq("identifier", id)));
			if (crates.size() == 0) {
				return null;
			}
			return crates.get(0);
		}
	}

	public boolean deleteCrate(int id) {
		synchronized (lock) {
			// Check if Bottle of to be deleted Crate still exists in the database
			final Crate crate = getCrateById(id);

			long deleted = crateCollection.deleteOne(eq("identifier", id)).getDeletedCount();
			if (deleted == 1) {
				return true;
			} else {
				return false;
			}
		}
	}

	public Crate addCrate(Crate newCrate) {
		synchronized (lock) {
			crateCollection.insertOne(new CrateDTO(newCrate));
			return this.getCrateById(newCrate.getId());
		}
	}

	public Crate updateCrate(int id, Crate updatedCrate) {
		synchronized (lock) {
			Document document = new Document();
			Optional.of(updatedCrate.getNoOfBottles()).ifPresent(d -> document.append("noOfBottles", d));
			Optional.of(updatedCrate.getPrice()).ifPresent(d -> document.append("price", d));
			Optional.of(updatedCrate.getInStock()).ifPresent(d -> document.append("inStock", d));

			crateCollection.updateOne(eq("identifier", id), new Document("$set", document));

			LOGGER.info("Updated Crate with" + id);
			return this.getCrateById(id);
		}
	}
}
