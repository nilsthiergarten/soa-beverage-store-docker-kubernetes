package de.uniba.dsg.jaxrs.get;

import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;
import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.Bottle;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Optional;
import java.util.logging.Logger;

@Path("bottles")
public class GetBottle {
    private static final Logger LOGGER = Logger.getLogger(GetBottle.class.getName());

    @GET
    @Path("{bottleId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBottle(@Context final UriInfo info, @PathParam("bottleId") final int id) {
        LOGGER.info("Get Bottle with id " + id);

        final Optional<Bottle> optionalBottle = DBClient.getInstance().getBottleById(id);

        if (optionalBottle.isPresent()) {
            final Bottle bottle = optionalBottle.get();
            return Response.ok(new BeverageDTO(bottle)).build();
        } else {
            LOGGER.warning(
                    String.format("Bottle with id %d could not be found because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorMessage(String.format("Bottle with id %d not found", id)).build();
        }
    }
}
