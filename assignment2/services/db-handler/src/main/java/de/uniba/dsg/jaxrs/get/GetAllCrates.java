package de.uniba.dsg.jaxrs.get;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("beverages")
public class GetAllCrates {

    private static final Logger LOGGER = Logger.getLogger(GetAllCrates.class.getName());

    @GET
    @Path("crates")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCrates() {
        LOGGER.info("Get all crates.");
        List<Beverage> crates = DBClient.getInstance().getCrates();
        LOGGER.info("Found " + crates.size() + " Crates ");
        BeverageListDTO beverageListDTO = new BeverageListDTO(BeverageDTO.marshallList(crates));

        return Response.ok(beverageListDTO).build();
    }
}
