package de.uniba.dsg.jaxrs.post;

import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.logging.Logger;

@Path("crates")
public class CreateCrate {
    private static final Logger LOGGER = Logger.getLogger(CreateCrate.class.getName());

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCrate(final CrateDTO crateDTO, @Context final UriInfo info) {
        LOGGER.info("Create new Crate.");
        if (crateDTO == null) {
            LOGGER.warning("Crate could not be created because Request Body was empty.");
            return ErrorResponseBuilder.notFound().uriInfo(info).verbPOST()
                    .errorType(ErrorType.MALFORMED_BODY)
                    .errorMessage("Request Body was empty").build();
        }

        // Bottle to be saved with new Crate already exists in the database
        int id = DBClient.getInstance().getCrates().stream().mapToInt(b -> ((Crate)b).getId()).max().orElse(0);
        Crate crate = crateDTO.convert();
        Bottle bottle = crate.getBottle();

        // Bottle to be saved with new Crate does not already exist in the database
        Bottle existingBottle = DBClient.getInstance().getBottleByName(bottle.getName());

        if(existingBottle == null){
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "Bottle not Found");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }
        LOGGER.info("Found bottle by name: "+ existingBottle);

        crate.setBottle(existingBottle);
        crate.setId(id + 1);
        Crate created = DBClient.getInstance().addCrate(crate);

        LOGGER.info("Crate with id " + crate.getId() + " successfully created.");

        UriBuilder path = info.getAbsolutePathBuilder();
        path.path(Integer.toString(created.getId()));
        return Response.created(path.build()).build();
    }
}
