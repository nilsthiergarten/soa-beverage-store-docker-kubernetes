package de.uniba.dsg.jaxrs.get;

import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;
import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.Crate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.logging.Logger;

@Path("crates")
public class GetCrate {
    private static final Logger LOGGER = Logger.getLogger(GetCrate.class.getName());

    @GET
    @Path("{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCrate(@Context final UriInfo info, @PathParam("crateId") final int id) {
        LOGGER.info("Get Crate with id " + id);
        final Crate crate = DBClient.getInstance().getCrateById(id);

        if (crate == null) {
            LOGGER.warning(
                    String.format("Crate with id %d could not be found because it does not exist.", id));
            return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorMessage(String.format("Crate with id %d not found", id)).build();
        }
        return Response.ok(new BeverageDTO(crate)).build();
    }



}
