package de.uniba.dsg.jaxrs.post;

import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;
import de.uniba.dsg.jaxrs.controller.DBClient;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.Bottle;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.logging.Logger;

@Path("bottles")
public class CreateBottle {
    private static final Logger LOGGER = Logger.getLogger(CreateBottle.class.getName());

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createBottle(final BottleDTO bottleDTO, @Context final UriInfo info) {
        LOGGER.info("Create new Bottle.");
        if (bottleDTO == null) {
            LOGGER.warning("Bottle could not be created because Request Body was empty.");
            return ErrorResponseBuilder.notFound().uriInfo(info).verbPOST()
                    .errorType(ErrorType.MALFORMED_BODY)
                    .errorMessage("Request Body was empty").build();
        }
        int id = DBClient.getInstance().getBottles().stream().mapToInt(b -> ((Bottle)b).getId()).max().orElse(0);

        Bottle bottle = bottleDTO.unmarshall();
        bottle.setId(id+1);
        Bottle created = DBClient.getInstance().addBottle(bottle);

        LOGGER.info("Bottle with id " + bottle.getId() + " and name " + bottle.getName() + " successfully created.");
        UriBuilder path = info.getAbsolutePathBuilder();
        path.path(Integer.toString(created.getId()));
        return Response.created(path.build()).build();
    }
}
