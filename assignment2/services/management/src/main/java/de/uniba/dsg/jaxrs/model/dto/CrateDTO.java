package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Crate;

import javax.validation.constraints.Min;
import javax.xml.bind.annotation.*;

import java.net.URI;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "crate")
@XmlType(propOrder = { "id", "price", "inStock", "bottle", "noOfBottles", "href" })
public class CrateDTO {
    @XmlElement(required = false)
    private int id;

    @XmlElement(required = true)
    private BottleDTO bottle;

    @XmlElement(required = true)
    private double price;

    @XmlElement(required = true)
    private int noOfBottles;

    @XmlElement(required = true)
    private int inStock;

    private URI href;

    public CrateDTO() {
    }

    public CrateDTO(final Crate crate, final URI baseUri) {
        this.id = crate.getId();
        this.bottle = new BottleDTO(crate.getBottle());
        this.noOfBottles = crate.getNoOfBottles();
        this.price = crate.getPrice();
        this.inStock = crate.getInStock();
    }

    public Crate unmarshall() {
        return new Crate(this.id, this.bottle.unmarshall(), this.noOfBottles, this.price, this.inStock);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BottleDTO getBottle() {
        return bottle;
    }

    public void setBottle(BottleDTO bottle) {
        this.bottle = bottle;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNoOfBottles() {
        return noOfBottles;
    }

    public void setNoOfBottles(int noOfBottles) {
        this.noOfBottles = noOfBottles;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public URI getHref() {
        return href;
    }

    public void setHref(URI href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "CrateDTO [" +
                "id=" + id +
                ", price=" + price +
                ", inStock=" + inStock +
                ", bottle=" + bottle +
                ", noOfBottles=" + noOfBottles +
                ", href=" + href +
                ']';
    }
}
