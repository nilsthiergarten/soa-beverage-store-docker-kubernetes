package de.uniba.dsg.jaxrs.controller;

import de.uniba.dsg.jaxrs.Configuration;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.*;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseException;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ManagementClient {

    private static final String BOTTLES_URI = "/bottles";
    private static final String CRATES_URI = "/crates";

    public static final ManagementClient instance = new ManagementClient();

    private final WebTarget baseUri;

    public ManagementClient() {
        Properties properties = Configuration.loadProperties();
        Client client = ClientBuilder.newClient();
        this.baseUri = client.target(properties.getProperty("DB_SERVER_URI"));
        System.out.println("[Management Service] BASE URI: " + baseUri);
    }

    private boolean deleteSingleBeverage(int id, String resourceUri) {
        final WebTarget target = baseUri.path(resourceUri + "/" + id);
        Response response = target.request().delete();
        if (response.getStatus() == 200) {
            System.out.println("Status " + response.getStatus());
            return true;
        } else {
            return false;
        }
    }

    // =============
    // == Bottles ==
    // =============

    public Bottle getBottle(final int id) throws ErrorResponseException {
        final WebTarget target = baseUri.path(BOTTLES_URI + "/" + id);

        try {
            return getRequestBuilder(target).get(BottleDTO.class).unmarshall();
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public boolean deleteBottle(final int id) throws ErrorResponseException {
        final WebTarget target = baseUri.path(BOTTLES_URI + "/" + id);

        try {
            Response response = target.request().delete();
            if (response.getStatus() == 200) {
                System.out.println("Status " + response.getStatus());
                return true;
            } else {
                return false;
            }
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public URI addBottle(final BottleDTO newBottle) throws ErrorResponseException {
        final WebTarget target = baseUri.path(BOTTLES_URI);

        try {
            final Response response = getRequestBuilder(target).post(Entity.json(newBottle), Response.class);
            return response.getLocation();
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public void updateBottle(final int id, final BottleDTO updatedBottle) throws ErrorResponseException {
        final WebTarget target = baseUri.path(BOTTLES_URI + "/" + id);

        try {
            getRequestBuilder(target).put(Entity.json(updatedBottle), BottleDTO.class);
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    // =============
    // == Crates ==
    // =============

    public Crate getCrate(final int id) throws ErrorResponseException {
        final WebTarget target = baseUri.path(CRATES_URI + "/" + id);

        try {
            return getRequestBuilder(target).get(CrateDTO.class).unmarshall();
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public boolean deleteCrate(final int id) throws ErrorResponseException {
        final WebTarget target = baseUri.path(CRATES_URI + "/" + id);
        try {
            Response response = getRequestBuilder(target).delete();
            return response.getStatus() == 200;
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public URI addCrate(final CratePostDTO newCrate) throws ErrorResponseException {
        final WebTarget target = baseUri.path(CRATES_URI);

        try {
            final Response response = getRequestBuilder(target).post(Entity.json(newCrate), Response.class);
            return  response.getLocation();
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    public void updateCrate(final int id, final CrateDTO updatedCrate) throws ErrorResponseException {
        final WebTarget target = baseUri.path(CRATES_URI + "/" + id);

        try {
            getRequestBuilder(target).put(Entity.json(updatedCrate), CrateDTO.class);
        }
        catch (ResponseProcessingException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (WebApplicationException e) {
            throw new ErrorResponseException(e.getResponse());
        }
        catch (Exception e) {
            Response response = ErrorResponseBuilder.internalServerError().verbGET().uriInfo(target.getUri())
                    .errorMessage(e.getMessage()).build();
            throw new ErrorResponseException(response);
        }
    }

    private Invocation.Builder getRequestBuilder(final WebTarget target) {
        return target.request(MediaType.APPLICATION_JSON);
    }
}
