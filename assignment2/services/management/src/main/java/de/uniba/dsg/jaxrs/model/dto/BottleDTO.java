package de.uniba.dsg.jaxrs.model.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.*;

import de.uniba.dsg.jaxrs.model.Bottle;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bottlePost")
@XmlType(propOrder = { "id","name", "price", "volume", "isAlcoholic", "volumePercent", "supplier", "inStock" })
public class BottleDTO {

    @XmlElement(required = false)
    private int id;


    @XmlElement(required = true)
    @NotBlank
    private String name;
    @XmlElement(required = true)
    @Min(value = 0)
    protected double price;
    @XmlElement(required = true)
    @Min(value = 0)
    private double volume;
    @XmlElement(required = true)
    private boolean isAlcoholic;
    @XmlElement(required = true)
    @Min(value = 0)
    private double volumePercent;
    @XmlElement(required = true)
    @NotBlank
    private String supplier;
    @XmlElement(required = false)
    @Min(value = 0)
    private int inStock;

    public BottleDTO() {
    }

    public BottleDTO(final Bottle bottle) {
        this.name = bottle.getName();
        this.price = bottle.getPrice();
        this.volume = bottle.getVolume();
        this.isAlcoholic = bottle.isAlcoholic();
        this.volumePercent = bottle.getVolumePercent();
        this.supplier = bottle.getSupplier();
        this.inStock = bottle.getInStock();
    }

    public static List<BottleDTO> marshall(final List<Bottle> bottles) {
        final ArrayList<BottleDTO> bottleDTOs = new ArrayList<>();
        for (final Bottle bttl : bottles) {
            bottleDTOs.add(new BottleDTO(bttl));
        }
        return bottleDTOs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public boolean isAlcoholic() {
        return isAlcoholic;
    }

    public void setAlcoholic(boolean isAlcoholic) {
        this.isAlcoholic = isAlcoholic;
    }

    public double getVolumePercent() {
        return volumePercent;
    }

    public void setVolumePercent(double volumePercent) {
        this.volumePercent = volumePercent;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BottlePostDTO [id="+id+"inStock=" + inStock + ", isAlcoholic=" + isAlcoholic + ", name=" + name + ", price="
                + price + ", supplier=" + supplier + ", volume=" + volume + ", volumePercent=" + volumePercent + "]";
    }

    public Bottle unmarshall() {
        return new Bottle(id, name, volume, isAlcoholic, volumePercent, price, supplier, inStock);
    }
}