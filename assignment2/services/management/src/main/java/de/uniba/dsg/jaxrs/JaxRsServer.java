package de.uniba.dsg.jaxrs;

import java.net.URI;
import java.util.Properties;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class JaxRsServer {
    private static Properties properties = Configuration.loadProperties();

    public static void main(String[] args) {
        String serverUri = properties.getProperty("SERVER_URI");

        URI baseUri = UriBuilder.fromUri(serverUri).build();
        ResourceConfig config = ResourceConfig.forApplicationClass(ManagementServiceApi.class);
        JdkHttpServerFactory.createHttpServer(baseUri, config);
        System.out.println("Management Server running at " +serverUri + " ready to serve your JAX-RS requests...");
    }
}
