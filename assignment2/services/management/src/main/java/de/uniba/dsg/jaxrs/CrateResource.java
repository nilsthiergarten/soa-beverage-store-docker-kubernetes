package de.uniba.dsg.jaxrs;

import java.net.URI;
import java.util.Optional;
import java.util.logging.Logger;

import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import de.uniba.dsg.jaxrs.controller.ManagementClient;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;
import de.uniba.dsg.jaxrs.model.dto.CratePostDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseException;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

@Path("beverages/crates")
public class CrateResource {

    private static final Logger LOGGER = Logger.getLogger(CrateResource.class.getName());
    @GET
    @Path("/{crateId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    public Response getCrate(@Context final UriInfo info, @PathParam("crateId") final int id) {

        LOGGER.info("Get Crate with id: " + id);

        try {
            final Crate crate = ManagementClient.instance.getCrate(id);

            if (crate == null) {
                LOGGER.warning(
                        String.format("Crate with id %d could not be found because it does not exist.", id));
                return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                        .errorType(ErrorType.RESOURCE_NOT_FOUND)
                        .errorMessage(String.format("Crate with id %d not found", id)).build();
            }

            return Response.ok(new BeverageDTO(crate, info.getBaseUri())).build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    @PUT
    @Path("/{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCrate(@Context final UriInfo info, @PathParam("crateId") final int id,
            final CrateDTO updatedCrate) {
        LOGGER.info("EMPLOYEE: Updating Crate " + updatedCrate);

        try {
            // Update Crate in database
            if (updatedCrate == null) {
                LOGGER.warning("Body was empty.");
                return ErrorResponseBuilder.notFound().uriInfo(info).verbPUT()
                        .errorType(ErrorType.MALFORMED_BODY)
                        .errorMessage("Body was empty.").build();
            }
            ManagementClient.instance.updateCrate(id, updatedCrate);

            return Response.ok().build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCrate(final CratePostDTO newCrate, @Context UriInfo info) {

        LOGGER.info("EMPLOYEE: Creating new Crate " + newCrate);

        try {
            // Create new Crate in database
            if (newCrate == null) {
                LOGGER.warning("Body was empty.");
                return ErrorResponseBuilder.notFound().uriInfo(info).verbPOST()
                        .errorType(ErrorType.MALFORMED_BODY)
                        .errorMessage("Body was empty.").build();
            }
            URI created = ManagementClient.instance.addCrate(newCrate);

            return Response.created(determineUri(created,info)).build();
        }
        catch (ErrorResponseException e)
        {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }

    }

    @DELETE
    @Path("{crateId}")
    public Response deleteCrateById(
            @Context final UriInfo info, @PathParam("crateId") int id) {
        LOGGER.info("EMPLOYEE: Deleting Crate with id " + id);

        try {
            // Delete Crate in database
            final boolean deleted = ManagementClient.instance.deleteCrate(id);

            if (!deleted) {
                LOGGER.warning(
                        String.format("Crate with id %d could not be deleted because it does not exist.", id));
                return ErrorResponseBuilder.notFound().uriInfo(info).verbDELETE()
                        .errorType(ErrorType.RESOURCE_NOT_FOUND)
                        .errorMessage(String.format("Crate with id %d not found", id)).build();
            }

            return Response.ok().build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    public URI determineUri(URI uri, UriInfo baseUri){

        String[] path = uri.getPath().split("/");

        return UriBuilder.fromUri(baseUri.getBaseUri()).path(CrateResource.class)
                .path(CrateResource.class, "getCrate").build(path[path.length-1]);

    }

}
