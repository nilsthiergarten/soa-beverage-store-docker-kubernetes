package de.uniba.dsg.jaxrs;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import de.uniba.dsg.jaxrs.controller.ManagementClient;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseException;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

@Path("beverages/bottles")

public class BottleResource {

    private static final Logger LOGGER = Logger.getLogger(BottleResource.class.getName());

    @GET
    @Path("/{bottleId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    public Response getBottle(@Context final UriInfo info, @PathParam("bottleId") final int id) {

        LOGGER.info("Get Bottle with id: " + id);

        try {
            final Bottle bottle = ManagementClient.instance.getBottle(id);

            if (bottle == null) {
                LOGGER.warning(
                        String.format("Bottle with id %d could not be found because it does not exist.", id));
                return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                        .errorType(ErrorType.RESOURCE_NOT_FOUND)
                        .errorMessage(String.format("Bottle with id %d not found", id)).build();
            }

            return Response.ok(new BeverageDTO(bottle, info.getBaseUri())).build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    @PUT
    @Path("/{bottleId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateBottle(@Context final UriInfo info, @PathParam("bottleId") final int id,
            final BottleDTO updatedBottle) {
        LOGGER.info("EMPLOYEE: Updating Bottle " + updatedBottle);

        try {
            // Update Bottle in database
            if (updatedBottle == null) {
                LOGGER.warning("Body was empty.");
                return ErrorResponseBuilder.notFound().uriInfo(info).verbPUT()
                        .errorType(ErrorType.MALFORMED_BODY)
                        .errorMessage("Body was empty.").build();
            }
            ManagementClient.instance.updateBottle(id, updatedBottle);
            return Response.ok().build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response createBottle(
            @Context final UriInfo info,
            @Valid @NotNull final BottleDTO newBottle) {
        LOGGER.info("EMPLOYEE: Creating new Bottle " + newBottle);

        try {
            // Create new Crate in database
            if (newBottle == null) {
                LOGGER.warning("Body was empty.");
                return ErrorResponseBuilder.notFound().uriInfo(info).verbPOST()
                        .errorType(ErrorType.MALFORMED_BODY)
                        .errorMessage("Body was empty.").build();
            }
            URI created = ManagementClient.instance.addBottle(newBottle);

            return Response.created(determineUri(created,info)).build();
        }
        catch (ErrorResponseException e)
        {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }

    }

    @DELETE
    @Path("/{bottleId}")
    public Response deleteBottle(@Context final UriInfo info, @PathParam("bottleId") final int id) throws ErrorResponseException {

        LOGGER.info("EMPLOYEE: Deleting Bottle with id " + id);

        try {
            // Delete Bottle in database
            final boolean deleted = ManagementClient.instance.deleteBottle(id);

            if (!deleted) {
                LOGGER.warning(
                        String.format("Bottle with id %d could not be deleted because it does not exist.", id));
                return ErrorResponseBuilder.notFound().uriInfo(info).verbDELETE()
                        .errorType(ErrorType.RESOURCE_NOT_FOUND)
                        .errorMessage(String.format("Bottle with id %d not found", id)).build();
            }
            System.out.println("Bottle with id "+ id + "deleted.");
            return Response.ok().build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }
    }

    public URI determineUri(URI uri, UriInfo baseUri){
        String[] path = uri.getPath().split("/");
        return UriBuilder.fromUri(baseUri.getBaseUri()).path(BottleResource.class)
                .path(BottleResource.class, "getBottle").build(path[path.length-1]);
    }

}
