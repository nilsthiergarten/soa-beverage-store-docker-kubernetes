package de.uniba.dsg.jaxrs.model.dto;

import java.net.URI;

import javax.validation.constraints.Min;
import javax.xml.bind.annotation.*;

import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "crate")
@XmlType(propOrder = { "id", "price", "inStock", "bottle", "noOfBottles", "href" })
public class CratePostDTO {

    private int id;

    @XmlElement(required = true)
    private BottleDTO bottle;
    @XmlElement(required = true)
    @Min(value = 0)
    private double price;
    @XmlElement(required = true)
    @Min(value = 1)
    private int noOfBottles;
    @XmlElement(required = true)
    @Min(value = 0)
    private int inStock;

    private URI href;

    public CratePostDTO() {
    }

    public CratePostDTO(final Crate crate, final URI baseUri) {
        this.id = crate.getId();
        this.bottle = new BottleDTO(crate.getBottle());
        this.noOfBottles = crate.getNoOfBottles();
        this.price = crate.getPrice();
        this.inStock = crate.getInStock();
    }

    public Crate unmarshall() {
        return new Crate(this.id, this.bottle.unmarshall(), this.noOfBottles, this.price, this.inStock);
    }

    @Override
    public String toString() {
        return "CrateDTO [" +
                "id=" + id +
                ", price=" + price +
                ", inStock=" + inStock +
                ", bottle=" + bottle +
                ", noOfBottles=" + noOfBottles +
                ", href=" + href +
                ']';
    }
}
