package de.uniba.dsg.jaxrs.get;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.controller.BeverageClient;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.logging.Logger;

@Path("beverages")
public class ReadAllBottles {

    private static final String baseUri = "http://db:9999/";
    private static final String getBeverageList = "beverages/bottles";

    private static final Logger LOGGER = Logger.getLogger(ReadAllBottles.class.getName());

    @GET
    @Path("bottles")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getBottles(@Context final UriInfo info,
                               @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit, @QueryParam("page")
                               @DefaultValue("1") final int page) {
        LOGGER.info("Get all Bottles. Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        // Parameter validation
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        final PaginationHelper<BeverageDTO> helper = new PaginationHelper<>(BeverageClient.instance.getAllBottles());
        final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                BeverageListDTO.marshallDTOList(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            LOGGER.warning("No Bottles found.");
            return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                    .errorType(ErrorType.RESOURCE_NOT_FOUND)
                    .errorMessage("No Bottles found.").build();
        }

        return Response.ok(response)
                .build();

    }

}