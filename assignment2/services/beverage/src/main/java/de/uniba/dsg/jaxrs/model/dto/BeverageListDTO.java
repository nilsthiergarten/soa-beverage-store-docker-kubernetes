package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "beverageList")
@XmlType(propOrder = { "beverages"})
public class BeverageListDTO {

    private List<BeverageDTO> beverages;

    public BeverageListDTO(List<BeverageDTO> beverages) {
        this.beverages = beverages;
    }

    public BeverageListDTO() {
    }

    public List<BeverageDTO> getBeverages() {
        return beverages;
    }

    public static List<BeverageDTO> marshallDTOList(final List<BeverageDTO> beverageList, final URI baseUri) {
        final ArrayList<BeverageDTO> beverages = new ArrayList<>();
        for (final BeverageDTO b : beverageList) {
            beverages.add(b);
            b.setHref(baseUri);
        }
        return beverages;
    }
}
