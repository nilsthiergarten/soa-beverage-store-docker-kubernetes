package de.uniba.dsg.jaxrs.model.error;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum(String.class)
public enum ErrorType {
    UNDEFINED,
    NOT_FOUND,
    INVALID_PARAMETER,
    MALFORMED_BODY,
    RESOURCE_NOT_FOUND,
    RESOURCE_UNMODIFIABLE,
    RESOURCE_UNDELETABLE,
    INSUFFICIENT_STOCK;
}
