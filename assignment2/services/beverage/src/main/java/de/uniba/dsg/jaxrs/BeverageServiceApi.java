package de.uniba.dsg.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import de.uniba.dsg.jaxrs.get.ReadAll;
import de.uniba.dsg.jaxrs.get.ReadAllBottles;
import de.uniba.dsg.jaxrs.get.ReadAllCrates;

@ApplicationPath("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BeverageServiceApi extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(ReadAll.class);
        resources.add(ReadAllBottles.class);
        resources.add(ReadAllCrates.class);

        return resources;
    }
}
