package de.uniba.dsg.jaxrs.model.error;

import javax.ws.rs.core.Response;

public class ErrorResponseException extends Exception
{
    private static final long serialVersionUID = -8745942419471969020L;

    private Response errorResponse;

    public ErrorResponseException(final Response errorResponse)
    {
        super(errorResponse.getEntity().toString());

        this.errorResponse = errorResponse;
    }

    public Response getErrorResponse()
    {
        return errorResponse;
    }

    public void setErrorResponse(Response errorResponse)
    {
        this.errorResponse = errorResponse;
    }

    @Override
    public String toString()
    {
        return "ErrorResponseException [errorResponse=" + errorResponse + "]";
    }
}