package de.uniba.dsg.jaxrs.get;

import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.controller.BeverageClient;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorMessage;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseBuilder;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseException;
import de.uniba.dsg.jaxrs.model.error.ErrorType;

@Path("beverages")
public class ReadAll {
    private static final Logger LOGGER = Logger.getLogger(ReadAll.class.getName());

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getBeverages(@Context final UriInfo info,
                                 @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit, @QueryParam("page")
                                 @DefaultValue("1") final int page) {
        LOGGER.info("Get all Beverages. Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        try {
            // Parameter validation
            if (pageLimit < 1 || page < 1) {
                final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                        "PageLimit or page is less than 1. Read the documentation for a proper handling!");
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
            }

            final PaginationHelper<BeverageDTO> helper = new PaginationHelper<>(BeverageClient.instance.getAllBeverages());
            final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                    BeverageListDTO.marshallDTOList(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

            if (helper.getPaginatedList().isEmpty()) {
                LOGGER.warning("No Beverages found.");
                return ErrorResponseBuilder.notFound().uriInfo(info).verbGET()
                        .errorType(ErrorType.RESOURCE_NOT_FOUND)
                        .errorMessage("No Beverages found.").build();
            }

            return Response.ok(response)
                    .build();
        }
        catch (ErrorResponseException e) {
            LOGGER.warning(e.getErrorResponse().getEntity().toString());
            return e.getErrorResponse();
        }

    }

}
