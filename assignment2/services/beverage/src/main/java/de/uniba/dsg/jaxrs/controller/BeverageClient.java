package de.uniba.dsg.jaxrs.controller;

import de.uniba.dsg.jaxrs.Configuration;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BeverageListDTO;
import de.uniba.dsg.jaxrs.model.error.ErrorResponse;
import de.uniba.dsg.jaxrs.model.error.ErrorResponseException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Properties;

public class BeverageClient {

    private static final String BEVERAGES_URI = "/beverages";
    private static final String BOTTLES_URI = "/beverages/bottles";
    private static final String CRATES_URI = "/beverages/crates";

    public static final BeverageClient instance = new BeverageClient();

    private final WebTarget baseUri;

    public BeverageClient() {
        Properties properties = Configuration.loadProperties();
        Client client = ClientBuilder.newClient();
        this.baseUri = client.target(properties.getProperty("DB_SERVER_URI"));
        System.out.println("[Beverage Service] BASE URI: " + baseUri);
    }

    public List<BeverageDTO> getAllBeverages() throws ErrorResponseException {
        final WebTarget target = baseUri.path(BEVERAGES_URI);
        System.out.println("[Beverage Service] GET ALL BEVERAGES: " + target.getUri());
        BeverageListDTO beverageList = target.request(MediaType.APPLICATION_JSON)
                .get(Response.class)
                .readEntity(BeverageListDTO.class);

        return beverageList.getBeverages();
    }

    public List<BeverageDTO> getAllBottles() {
        final WebTarget target = baseUri.path(BOTTLES_URI);
        System.out.println("[Beverage Service] GET ALL BOTTLES: " + target.getUri());
        BeverageListDTO bottlesList = target.request(MediaType.APPLICATION_JSON)
                .get(Response.class)
                .readEntity(BeverageListDTO.class);

        return bottlesList.getBeverages();
    }

    public List<BeverageDTO> getAllCrates() {
        final WebTarget target = baseUri.path(CRATES_URI);
        System.out.println("[Beverage Service] GET ALL CRATES: " + target.getUri());
        BeverageListDTO cratesList = target.request(MediaType.APPLICATION_JSON)
                .get(Response.class)
                .readEntity(BeverageListDTO.class);
        return cratesList.getBeverages();
    }
}
