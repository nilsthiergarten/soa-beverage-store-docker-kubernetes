package de.uniba.dsg.jaxrs.model.error;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "errorResponse")
@XmlType(propOrder = { "errorMessage", "path", "verb" })
public class ErrorResponse
{
    @XmlElement(required = true) private ErrorMessage errorMessage;
    @XmlElement(required = false) private String path;
    @XmlElement(required = false) private String verb;

    public ErrorResponse()
    {
    }

    public ErrorResponse(ErrorMessage errorMessage)
    {
        this(errorMessage, "undefined", "undefined");
    }

    public ErrorResponse(ErrorMessage errorMessage, String path, String verb)
    {
        this.errorMessage = errorMessage;
        this.path = path;
        this.verb = verb;
    }

    public ErrorMessage getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public String getVerb()
    {
        return verb;
    }

    public void setVerb(String verb)
    {
        this.verb = verb;
    }

    @Override
    public String toString()
    {
        return "ErrorResponse [errorMessage=" + errorMessage + ", path=" + path + ", verb=" + verb + "]";
    }

    public Response toResponse(Status status)
    {
        return Response.status(status).entity(this).type("application/json").build();
    }
}