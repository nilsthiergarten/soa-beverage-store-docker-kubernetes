package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;

import de.uniba.dsg.jaxrs.util.BeverageType;

import javax.xml.bind.annotation.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "beverage")
@XmlType(propOrder = { "id", "name", "type", "bottle", "noOfBottles", "volume",
        "isAlcoholic", "volumePercent", "price", "supplier", "inStock" ,"href"})
public class BeverageDTO {

    //Interface
    @XmlElement(required = false)
    private int id;
    @XmlElement(required = true)
    private String name;
    @XmlElement(required = true)
    private Double price;
    private Integer inStock;
    private URI href;
    //Type of beverage
    @XmlElement(required = true)
    private BeverageType type;

    //Bottle
    private Double volume;
    private Boolean isAlcoholic;
    private Double volumePercent;
    private String supplier;

    //Crate
    private String bottle;
    private Integer noOfBottles;

    public BeverageDTO(final Bottle bottle, final URI baseUri) {
        this.type = BeverageType.BOTTLE;
        this.id = bottle.getId();
        this.name = bottle.getName();
        this.volume = bottle.getVolume();
        this.isAlcoholic = bottle.isAlcoholic();
        this.volumePercent = bottle.getVolumePercent();
        this.price = bottle.getPrice();
        this.supplier = bottle.getSupplier();
        this.inStock = bottle.getInStock();
    }

    public BeverageDTO(final Crate crate, final URI baseUri) {
        this.type = BeverageType.CRATE;
        this.id = crate.getId();
        this.bottle = crate.getBottle().getName();
        this.noOfBottles = crate.getNoOfBottles();
        this.price = crate.getPrice();
        this.inStock = crate.getInStock();
    }

    public BeverageDTO() {
    }

    // BEVERAGE
    public static List<BeverageDTO> marshallList(final List<Beverage> beverageList, final URI baseUri) {
        final ArrayList<BeverageDTO> beverages = new ArrayList<>();
        for (final Beverage beverage : beverageList) {
            if(beverage instanceof Crate){
                beverages.add(new BeverageDTO((Crate)beverage, baseUri));
            }else{
                beverages.add(new BeverageDTO((Bottle) beverage, baseUri));
            }
        }
        return beverages;
    }

    public static BeverageDTO marshall(final Beverage beverage, final URI baseUri) {
        if(beverage instanceof Crate){
            return new BeverageDTO((Crate)beverage, baseUri);
        }else{
            return new BeverageDTO((Bottle) beverage, baseUri);
        }
    }


    // BOTTLE
    public static List<BeverageDTO> marshallBottles(final List<Bottle> bottleList, final URI baseUri) {
        final ArrayList<BeverageDTO> bottles = new ArrayList<>();
        for (final Bottle bottle : bottleList) {
            bottles.add(new BeverageDTO(bottle, baseUri));
        }
        return bottles;
    }

    // CRATE
    public static List<BeverageDTO> marshallCrates(final List<Crate> crateList, final URI baseUri) {
        final ArrayList<BeverageDTO> crates = new ArrayList<>();
        for (final Crate crate : crateList) {
            crates.add(new BeverageDTO(crate, baseUri));
        }
        return crates;
    }

    public void setHref(URI href) {
        this.href = href;
    }
}
