# SOA-Assignment 2 - Group 7

**Sridhara Madhu Mohan Nelemane - Stefan Raab - Anne Schwarz - Nils Thiergarten**

## Part 1: Docker

Start the docker engine first.
Be on path `$ /group7/`.

Go to the services directory and start docker-compose:

```shell
$ cd assignment2/services/
$ docker-compose up
```

Open the Insomnia-Tests to access the REST-API or test the API in the browser. For example type in the following to get all 
beverages from the customer-site: `http://127.0.0.1:8080/v1/beverages`.
<br>
<br>The beverage-service is available at `http://localhost:8080/v1`.
<br>The management-service is available at `http://localhost:8090/v1`.

## Part 2: Kubernetes

**Step 1**

Be on path `$ /group7/`.

Check if the Docker-images are available:
```shell
$ docker image ls
```
If you see the following images, go on to step 2.
```shell
services_management
services_beverage  
services_db-handler
```
If not, execute the following commands:

```shell
$ cd assignment2/services/
$ eval $(minikube docker-env)
$ docker-compose build
```

Go to step 2.

**Step 2**

Type the following commands on path ```$ /group7/assignment2/```:

```shell
$ minikube start
$ cd architecture
$ kubectl apply -f .
```

Wait a few minutes for all pods to start and check if they are running:

```shell
$ kubectl get pods
```

A table similar to the following should be displayed:

```shell
NAME                                  READY   STATUS    RESTARTS   AGE
beverage-service-dfffcb4ff-nzqzw      1/1     Running   0          30m
db-54994cd598-69mdz                   1/1     Running   0          30m
management-service-58bbb8df4d-lrcvz   1/1     Running   0          30m
mongodb-0                             1/1     Running   0          30m
```

If the status is `ErrImageNeverPull` as shown in the table below, you need to rebuild the images by executing the following commands on path ```/group7/assignment2/services```:

```shell
$ eval $(minikube docker-env)
$ docker-compose build
```

```shell
NAME                                  READY   STATUS              RESTARTS   AGE
beverage-service-dfffcb4ff-b4xvc      0/1     ErrImageNeverPull   0          51s
db-54994cd598-4pchk                   0/1     ErrImageNeverPull   0          51s
management-service-58bbb8df4d-crg7b   0/1     ErrImageNeverPull   0          51s
mongodb-0                             0/1     ContainerCreating   0          51s
```

**Step 3**

Get the URLs of the services:
```shell
$ minikube service management-service
$ minikube service beverage-service
```

A table similar to this is displayed:
```shell
|-----------|------------------|-------------|---------------------------|
| NAMESPACE |       NAME       | TARGET PORT |            URL            |
|-----------|------------------|-------------|---------------------------|
| default   | beverage-service |        8080 | http://192.168.49.2:30453 |
|-----------|------------------|-------------|---------------------------|
🏃  Starting tunnel for service beverage-service.
|-----------|------------------|-------------|------------------------|
| NAMESPACE |       NAME       | TARGET PORT |          URL           |
|-----------|------------------|-------------|------------------------|
| default   | beverage-service |             | http://127.0.0.1:59158 |
|-----------|------------------|-------------|------------------------|

```
To test the REST APIs, open the Insomnia file and change the URL to the bottom URL specified in the table. In this case `http://127.0.0.1:59158`.
<br>
<br>It is also possible to test the API in the browser. For example, request all beverages as a customer:
<br>`http://127.0.0.1:59158/v1/beverages`.

The requests may take some time in the beginning.
