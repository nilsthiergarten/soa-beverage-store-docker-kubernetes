# JAX-RS REST API - SOA Assignment 1 Summer Term 2021

Implement a beverage store as specified in the assignment description.
Your project must be buildable with gradle (the easiest way to achieve this is using this template).
If your project can't be run with `gradlew run` to start the JAX-RS server, your submission will be marked with 0 points.
The server is available at `localhost:9999/v1` per default. 
This can be configured via `src/main/resources/config.properties`.
 
To discover the different resources, methods and data schemas use the [Swagger Editor](https://editor.swagger.io/#) and the `openapi.yaml` file.
Also include a swagger UI resource to enable displaying swagger UI as in our demo project.
## Swagger UI
In addition, Swagger UI is added to this project and you can access it at: http://localhost:9999/v1/swagger-ui/index.html?url=openapi.yaml per default.
## Artefacts, you have to submit
- Source Code
- openapi.yaml
- insomnia.json
