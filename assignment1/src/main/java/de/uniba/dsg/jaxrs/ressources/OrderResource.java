package de.uniba.dsg.jaxrs.ressources;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.model.*;
import de.uniba.dsg.jaxrs.controller.BeverageService;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.api.PaginatedOrders;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.OrderDTO;
import de.uniba.dsg.jaxrs.model.dto.OrderItemDTO;

import org.glassfish.jersey.server.Uri;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import java.util.List;
import java.util.logging.Logger;

@Path("orders")
public class OrderResource {

    private static final Logger logger = Logger.getLogger("OrderResource");

    @GET
    @Path("employee")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getOrders(@Context final UriInfo info,
            @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit, @QueryParam("page")
    @DefaultValue("1") final int page) {
        logger.info("Get all Orders (Employee view). Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        // Parameter validation
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        final PaginationHelper<Order> helper = new PaginationHelper<>(BeverageService.instance.getAllOrders());
        final PaginatedOrders response = new PaginatedOrders(helper.getPagination(info, page, pageLimit),
                OrderDTO.marshall(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottles found.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok(response)
                .build();
    }

    @PUT
    @Path("order/employee/{orderId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response processOrder(@PathParam("orderId") final int id) {
        logger.info("Create New Order with Items");
        final Order order = BeverageService.instance.getOrder(id);
        if (order == null) {
            logger.info("No order found for id: " + id + ". Order cannot be deleted.");
            final ErrorMessage errorMessage =
                    new ErrorMessage(ErrorType.NOT_FOUND, "No order found for id: " + id + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
        Response response = null;
        switch (order.getStatus()) {
        case PROCESSED:
            logger.info("Order already processed. Nothing to be done");
            response = Response.status(Response.Status.NOT_MODIFIED).build();
            break;

        case SUBMITTED:
            final boolean processed = BeverageService.instance.processOrder(id);
            logger.info("Order with ID " + id + " is PROCESSED");
            response = Response.ok().build();
            System.out.println("Response Submitted " + response);

        }

        return response;
    }

    @GET
    @Path("order/{orderId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    public Response getOrder(@Context final UriInfo info, @PathParam("orderId") final int id) {
        logger.info("Get order with id " + id);
        final Order order = BeverageService.instance.getOrder(id);
        if (order == null) {
            logger.info("No order found for id: " + id + ".");
            final ErrorMessage errorMessage =
                    new ErrorMessage(ErrorType.NOT_FOUND, "No order found for id: " + id + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        logger.info("Order: " + order);
        System.out.println("OrderItems: " + order.getPositions());

        return Response.ok(new OrderDTO(order, info.getBaseUri())).build();
    }

    @POST
    @Path("order")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(final OrderDTO orderDTO, @Context final UriInfo uriInfo,
            @QueryParam("beverageId") final int beverageId, @QueryParam("quantity") final int quantity,
            @QueryParam("beverageType") final String beverageType) {
        logger.info("Add Beverage to new order.");

        Beverage newBeverage = null;
        double price = 0.0;
        Response response = null;

        switch (beverageType) {
        case ("bottle"):
            Bottle bottle = BeverageService.instance.getBottle(beverageId);

            if (bottle == null) {
                logger.info("No bottle found for id: " + beverageId + ".");
                final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND,
                        "No " + beverageType + " found for id: " + beverageId + ".");
                return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
            }

            newBeverage = bottle;
            price = bottle.getPrice();
            System.out.println("Price: " + price);
            if (quantity > bottle.getInStock()) {
                final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                        "Only " + bottle.getInStock() + " " + bottle.getName()
                                + " are available. Decrease the quantity.");
                response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                return response;
            } else {
                BeverageService.instance.addOrder(bottle, quantity, price);
            }

            break;
        case ("crate"):
            Crate crate = BeverageService.instance.getCrate(beverageId);
            if (crate == null) {
                logger.info("No crate found for id: " + beverageId + ".");
                final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND,
                        "No" + beverageType + " found for id: " + beverageId + ".");
                return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
            }
            newBeverage = crate;
            price = crate.getPrice();
            if (quantity > crate.getInStock()) {
                final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                        "Only " + crate.getInStock() + " Crates with " + crate.getBottle().getName()
                                + " are available. Decrease the quantity.");
                response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                return response;
            } else {
                BeverageService.instance.addOrder(crate, quantity, price);
            }

            break;
        }

        if (newBeverage == null) {
            logger.info("No beverage found for id: " + beverageId + ". Order cannot be created");
            final ErrorMessage errorMessage =
                    new ErrorMessage(ErrorType.NOT_FOUND, "No beverafe found for id: " + beverageId + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        } else {
            response = Response.ok().build();
        }

        return response;
    }

    @DELETE
    @Path("order/{orderId}/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSpecificOrder(@PathParam("orderId") final int id) {
        logger.info("Delete order with id " + id);

        //check if an order hasn’t been processed

        final Order order = BeverageService.instance.getOrder(id);
        if (order == null) {
            logger.info("No order found for id: " + id + ". Order cannot be deleted.");
            final ErrorMessage errorMessage =
                    new ErrorMessage(ErrorType.NOT_FOUND, "No order found for id: " + id + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        Response response = null;
        switch (order.getStatus()) {
        case PROCESSED:
            logger.info("Order already processed");
            response = Response.status(Response.Status.FORBIDDEN).build();
            break;
        case SUBMITTED:
            final boolean deleted = BeverageService.instance.deleteOrder(id);
            logger.info("Order with ID " + id + " is submitted + will be deleted");
            response = Response.ok().build();
            System.out.println("Response Submitted: " + response);

        }
        return response;

    }

    // delete Position from order
    @DELETE
    @Path("order/{orderId}/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePosition(@Context final UriInfo uriInfo, @PathParam("orderId") final int id,
            @PathParam("number") final int number, final OrderItemDTO deletedPosition) {
        logger.info("Delete OrderItem " + number + " in Order with id " + id);

        final Order order = BeverageService.instance.getOrder(id);
        final OrderItem position = BeverageService.instance.getOrderItem(id, number);

        System.out.println("Positions in Delete: " + position);

        Response response = null;
        switch (order.getStatus()) {
        case PROCESSED:
            logger.info("Order already processed");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.FORBIDDEN,
                    "Order " + id + " is already processed. No update could be performed.");
            response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage).build();
            System.out.println("Response Processed: " + response);
            break;
        case SUBMITTED:

            logger.info("Order is submitted + can be deleted");

            final boolean deleted = BeverageService.instance.deleteOrderItem(id, number);

            if (!deleted) {
                logger.info("No Position found for number: " + number + ". Position cannot be deleted.");
                final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                        "No Position found for number: " + number + ". Position cannot be deleted.");
                response = Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
            } else {
                logger.info("Position " + number + " in Order " + id + " successfully deleted.");
                response = Response.ok().build();
            }
        }
        return response;

    }

    @POST
    @Path("order/edit/{orderId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addBeverage(@Context final UriInfo uriInfo, @PathParam("orderId") final int id,
            @QueryParam("beverageId") final int beverageId, @QueryParam("quantity") final int quantity,
            @QueryParam("beverageType") final String beverageType) {
        logger.info("Add Bottle in Order with id " + id);
        final Order order = BeverageService.instance.getOrder(id);
        Response response = null;
        if (order == null) {
            logger.info("Order with id " + id + " not found.");
            final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                    "No Order found for id: " + id + ". Position cannot be added.");
            response = Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
            return response;
        }

        Beverage addedBeverage = null;
        switch (order.getStatus()) {
        case PROCESSED:
            logger.info("Order already processed");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.FORBIDDEN,
                    "Order " + id + " is already processed. No update could be performed.");
            response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage).build();
            break;
        case SUBMITTED:
            //final boolean deleted = BeverageService.instance.deleteOrder(id);
            logger.info("Order is submitted + can be edited");
            if (beverageType.equals("bottle")) {
                final Bottle bottle = BeverageService.instance.getBottle(beverageId);
                addedBeverage = bottle;
                if (bottle == null) {
                    logger.info("No bottle found for id: " + beverageId + ".");
                    final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                            "No " + beverageType + " found for id: " + beverageId + ".");
                    return Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
                }

                if (quantity > bottle.getInStock()) {
                    System.out.println("Bottle in Stock: " + bottle.getInStock());
                    final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                            "Only " + bottle.getInStock() + " " + bottle.getName()
                                    + " are available. Decrease the quantity.");
                    response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                    return response;
                }

            } else {
                final Crate crate = BeverageService.instance.getCrate(beverageId);
                addedBeverage = crate;
                if (crate == null) {
                    logger.info("No crate found for id: " + beverageId + ".");
                    final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                            "No " + beverageType + " found for id: " + beverageId + ".");
                    return Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
                }
                if (quantity > crate.getInStock()) {
                    System.out.println("Bottle in Stock: " + crate.getInStock());
                    final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                            "Only " + crate.getInStock() + " Crates with " + crate.getBottle().getName()
                                    + " are available. Decrease the quantity.");
                    response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                    return response;
                }

            }

            final boolean resultOrder = BeverageService.instance.addBeverageToOrder(id, addedBeverage, quantity);

            if (!resultOrder) {
                logger.info("No order found for id: " + id + ". Position cannot be added.");
                final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                        "No Order found for id: " + id + ". Position cannot be added.");
                response = Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
            } else {
                logger.info("Position " + addedBeverage + " in order" + id + "successfully added.");
                response = Response.ok().build();
            }
            break;

        }
        return response;

    }

    @DELETE
    @Path("order/edit/{orderId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteBeverage(@Context final UriInfo uriInfo, @PathParam("orderId") final int id,
            @QueryParam("beverageId") final int beverageId, @QueryParam("quantity") final int quantity,
            @QueryParam("beverageType") final String beverageType) {
        logger.info("Delete Bottle in Order with id " + id);
        final Order order = BeverageService.instance.getOrder(id);
        Response response = null;
        if (order == null) {
            logger.info("Order with id " + id + " not found.");
            final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                    "No Order found for id: " + id + ". Position cannot be added.");
            response = Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
            return response;
        }

        Beverage deletedBeverage = null;
        final boolean resultOrder;
        switch (order.getStatus()) {
        case PROCESSED:
            logger.info("Order already processed");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.FORBIDDEN,
                    "Order " + id + " is already processed. No update could be performed.");
            response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage).build();
            System.out.println("Response Processed: " + response);
            break;
        case SUBMITTED:
            //final boolean deleted = BeverageService.instance.deleteOrder(id);
            logger.info("Order is submitted + can be edited");

            if (beverageType.equals("bottle")) {

                final Bottle bottle = BeverageService.instance.getBottle(beverageId);
                deletedBeverage = bottle;

                if (bottle == null) {
                    logger.info("No bottle found for id: " + beverageId + ".");
                    final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                            "No " + beverageType + " found for id: " + beverageId + ".");
                    return Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
                }

                if (quantity > bottle.getInStock()) {

                    final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                            "Only " + bottle.getInStock() + " " + bottle.getName()
                                    + " are available. Decrease  quantity.");
                    response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                    return response;
                }
                resultOrder = BeverageService.instance.deleteBeverageFromOrder(id, deletedBeverage, quantity);

            } else {
                final Crate crate = BeverageService.instance.getCrate(beverageId);
                deletedBeverage = crate;

                if (crate == null) {
                    logger.info("No crate found for id: " + beverageId + ".");
                    final ErrorMessage errorMessageNotFound = new ErrorMessage(ErrorType.NOT_FOUND,
                            "No " + beverageType + " found for id: " + beverageId + ".");
                    return Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
                }

                if (quantity > crate.getInStock()) {

                    final ErrorMessage errorMessage2 = new ErrorMessage(ErrorType.FORBIDDEN,
                            "Only " + crate.getInStock() + " Crates with " + crate.getBottle().getName()
                                    + " are available. Decrease the quantity.");
                    response = Response.status(Response.Status.FORBIDDEN).entity(errorMessage2).build();
                    return response;
                }
                resultOrder = BeverageService.instance.deleteBeverageFromOrder(id, deletedBeverage, quantity);

            }

            if (!resultOrder) {
                logger.info("No order found for id: " + id + ". Position cannot be added.");
                final ErrorMessage errorMessageNotFound =
                        new ErrorMessage(ErrorType.NOT_FOUND, "No Beverage found. Position cannot be deleted.");
                response = Response.status(Response.Status.NOT_FOUND).entity(errorMessageNotFound).build();
            } else {
                logger.info("Position " + deletedBeverage + " in order" + id + "successfully deleted.");
                response = Response.ok().build();
            }

        }
        return response;
    }

}
