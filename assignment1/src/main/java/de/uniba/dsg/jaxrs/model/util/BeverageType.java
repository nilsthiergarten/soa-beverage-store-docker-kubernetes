package de.uniba.dsg.jaxrs.model.util;

public enum BeverageType {
    BOTTLE,CRATE;
}
