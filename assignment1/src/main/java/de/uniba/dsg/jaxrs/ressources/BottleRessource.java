package de.uniba.dsg.jaxrs.ressources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.controller.BeverageService;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.ErrorMessage;
import de.uniba.dsg.jaxrs.model.ErrorType;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;

@Path("beverages/bottles")
public class BottleRessource {
    private static final Logger logger = Logger.getLogger("BottleRessource");

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBottles(@Context final UriInfo info,
                              @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit,
                              @QueryParam("page") @DefaultValue("1") final int page) {
        logger.info("Get all bottles. Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        // Parameter validation
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER, "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        final PaginationHelper<Bottle> helper = new PaginationHelper<>(BeverageService.instance.getAllBottles());
        final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                BeverageDTO.marshallBottles(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottles found.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok(response).build();
    }

    @GET
    @Path("{bottleId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getBottle(@Context final UriInfo info, @PathParam("bottleId") final int id) {
        logger.info("Get Bottle with id " + id);
        final Bottle bottle = BeverageService.instance.getBottle(id);

        if (bottle == null) {
            logger.info("No Bottle found for id: " + id + ".");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottle found for id: " + id + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
        return Response.ok(new BeverageDTO(bottle, info.getBaseUri())).build();
    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchForBottleName(
            @Context final UriInfo uriInfo, @QueryParam("name") final String name) {
        logger.info("SEARCH: Search for bottle with name " + name);

        final Bottle bottle = BeverageService.instance.getBottleByName(name);

        if (bottle == null) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "SEARCH: No Bottle found for name: " + name + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok().entity(new BeverageDTO(bottle, uriInfo.getBaseUri())).build();
    }

    @POST
    @Path("/employee")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBottle(final BottleDTO bottleDTO, @Context final UriInfo uriInfo) {
        logger.info("Create new Bottle.");
        if (bottleDTO == null) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "Request Body was empty");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }
        final Bottle bottle = bottleDTO.unmarshall();

        BeverageService.instance.addBottle(bottle);

        logger.info("Bottle with id " + bottle.getId() + " and name " + bottle.getName() + " successfully created.");
        return Response.created(
                UriBuilder.fromUri(uriInfo.getBaseUri()).path(BottleRessource.class)
                        .path(BottleRessource.class, "getBottle").build(bottle.getId())).build();
    }

    @PUT
    @Path("employee/{bottleId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBottle(@Context final UriInfo uriInfo, @PathParam("bottleId") final int id, final BottleDTO updatedBottle) {
        logger.info("Update Bottle with id " + id + ".");
        if (updatedBottle == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage(ErrorType.INVALID_PARAMETER, "Body was empty")).build();
        }

        final Bottle bottle = BeverageService.instance.getBottle(id);

        if (bottle == null) {
            logger.info("No Bottle found for id: " + id + ". Bottle cannot be updated.");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottle found for id: " + id + ". No update could be performed.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        final Bottle resultBottle = BeverageService.instance.updateBottle(id, updatedBottle.unmarshall());

        logger.info("Bottle with id " + resultBottle.getId() + " and name " + resultBottle.getName() + " successfully updated.");
        return Response.ok().entity(new BeverageDTO(resultBottle, uriInfo.getBaseUri())).build();
    }

    @DELETE
    @Path("employee/{bottleId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSpecificBottle(@PathParam("bottleId") final int id) {
        logger.info("Delete Bottle with id " + id);
        final boolean deleted = BeverageService.instance.deleteBottle(id);

        if (!deleted) {
            logger.info("No Bottle found for id: " + id + ". Bottle cannot be deleted.");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottle found for id: " + id + ". Bottle cannot be deleted.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        } else {
            logger.info("Bottle with id " + id + " successfully deleted.");
            return Response.ok().build();
        }
    }
}
