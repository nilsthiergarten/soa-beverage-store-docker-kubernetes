package de.uniba.dsg.jaxrs.model.api;

import java.net.URI;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.uniba.dsg.jaxrs.model.dto.OrderDTO;

@XmlRootElement
@XmlType(propOrder = {"pagination", "orderDTOS", "href"})
public class PaginatedOrders {

    private Pagination pagination;
    private List<OrderDTO> orderDTOS;
    private URI href;

    public PaginatedOrders(Pagination pagination, List<OrderDTO> orderDTOS, URI href) {
        this.pagination = pagination;
        this.orderDTOS = orderDTOS;
        this.href = href;
    }

    public PaginatedOrders() {
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<OrderDTO> getOrderDTOS() {
        return orderDTOS;
    }

    public void setOrderDTOS(List<OrderDTO> orderDTOS) {
        this.orderDTOS = orderDTOS;
    }

    public URI getHref() {
        return href;
    }

    public void setHref(URI href) {
        this.href = href;
    }
}