package de.uniba.dsg.jaxrs.ressources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.glassfish.jersey.internal.Errors;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.controller.BeverageService;
import de.uniba.dsg.jaxrs.model.*;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.BottleDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;

@Path("beverages")
public class BeverageRessource {

    private static final Logger logger = Logger.getLogger("BeverageResource");

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getBeverages(@Context final UriInfo info,
            @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit, @QueryParam("page")
    @DefaultValue("1") final int page) {
        logger.info("Get all Beverages. Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        // Parameter validation
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        final PaginationHelper<Beverage> helper = new PaginationHelper<>(BeverageService.instance.getAllBeverages());
        final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                BeverageDTO.marshallList(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Bottles found.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok(response)
                .build();

    }

    @GET
    @Path("filter")
    @Produces(MediaType.APPLICATION_JSON)
    public Response filterBeverageByPrice(@Context final UriInfo info,
            @QueryParam("minPrice") @DefaultValue("0") final int minPrice, @QueryParam("maxPrice")
    @DefaultValue("100") final int maxPrice,@QueryParam("pageLimit") @DefaultValue("10") final int pageLimit,
            @QueryParam("page")
            @DefaultValue("1") final int page) {

        logger.info("FILTER: Filter Beverages by minPrice " + minPrice + " and maxPrice" + maxPrice + ".");

        if (minPrice < 0 || maxPrice < 0) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "minPrice or maxPrice is less than 0. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER,
                    "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        PaginationHelper<BeverageDTO> helper = new PaginationHelper<>(
                BeverageDTO.marshallList(BeverageService.instance.filterBeveragesByPrice(minPrice, maxPrice),
                        info.getBaseUri()));
        final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                helper.getPaginatedList(), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "FILTER: No Bottles found for filter parameters minPrice " + minPrice + " and maxPrice " + maxPrice + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok(response)
                .build();
    }


}
