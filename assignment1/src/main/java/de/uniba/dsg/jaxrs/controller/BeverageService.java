package de.uniba.dsg.jaxrs.controller;

import de.uniba.dsg.jaxrs.db.DB;
import de.uniba.dsg.jaxrs.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BeverageService {

    public static final BeverageService instance = new BeverageService();

    private final DB db;

    public BeverageService() {
        this.db = new DB();
    }

    public List<Beverage> getAllBeverages() {
        return db.getAllBeverages();
    }

    public List<Beverage> filterBeveragesByPrice(final int minPrice, final int maxPrice) {
        List<Beverage> beverages = new ArrayList<>();
        beverages.addAll(this.db.getCrates().stream().filter(e -> e.getPrice() > minPrice && e.getPrice() < maxPrice).collect(
                Collectors.toList()));
        beverages.addAll(this.db.getBottles().stream().filter(e -> e.getPrice() > minPrice && e.getPrice() < maxPrice).collect(
                Collectors.toList()));

        return beverages;
    }

    // BEVERAGE
    public void decreaseQuantitiesInBeverage(Beverage beverage, final int quantities) {
        if (beverage instanceof Crate) {
            Crate crate = (Crate) beverage;
            crate.setInStock(crate.getInStock() - quantities);
            System.out.println("Quantities in update: " + crate.getInStock());
            updateCrate(((Crate) beverage).getId(), (Crate) beverage);
        } else {
            Bottle bottle = (Bottle) beverage;
            bottle.setInStock(bottle.getInStock() - quantities);
            updateBottle(((Bottle) beverage).getId(), (Bottle) beverage);
        }
    }

    public void increaseQuantitiesInBeverage(Beverage beverage, final int quantities) {
        if (beverage instanceof Crate) {
            Crate crate = (Crate) beverage;
            crate.setInStock(crate.getInStock() + quantities);
            System.out.println("Quantities in update: " + crate.getInStock());
            updateCrate(((Crate) beverage).getId(), (Crate) beverage);
        } else {
            Bottle bottle = (Bottle) beverage;
            bottle.setInStock(bottle.getInStock() + quantities);
            updateBottle(((Bottle) beverage).getId(), (Bottle) beverage);
        }
    }

    // BOTTLE
    public List<Bottle> getAllBottles() {
        return db.getBottles();
    }

    public Bottle getBottle(final int id) {
        return this.db.getBottle(id);
    }

    public boolean deleteBottle(final int id) {
        return this.db.deleteBottle(id);
    }

    public Bottle addBottle(final Bottle newBottle) {
        if (newBottle == null) {
            return null;
        }

        this.db.addBottleToDb(newBottle);
        return newBottle;
    }

    public Bottle updateBottle(final int id, final Bottle updatedBottle) {

        final Bottle bottle = this.db.getBottle(id);

        if (bottle == null || updatedBottle == null) {
            return null;
        }
        Optional.ofNullable(updatedBottle.getName()).ifPresent(bottle::setName);
        if (updatedBottle.getVolume() != 0.0) bottle.setVolume(updatedBottle.getVolume());
        Optional.ofNullable(updatedBottle.isAlcoholic()).ifPresent(bottle::setAlcoholic);
        if (updatedBottle.getVolumePercent() != 0.0) bottle.setVolumePercent(updatedBottle.getVolumePercent());
        if (updatedBottle.getPrice() != 0) bottle.setPrice(updatedBottle.getPrice());
        Optional.ofNullable(updatedBottle.getSupplier()).ifPresent(bottle::setSupplier);
        if (updatedBottle.getInStock() != 0) bottle.setInStock(updatedBottle.getInStock());

        return bottle;
    }

    public Bottle getBottleByName(final String name) {
        return this.db.getBottleByName(name);
    }

    // CRATE
    public List<Crate> getAllCrates() {
        return db.getCrates();
    }

    public Crate getCrate(final int id) {
        return this.db.getCrate(id);
    }

    public boolean deleteCrate(final int id) {
        return this.db.deleteCrate(id);
    }

    public Crate addCrate(final Crate newCrate) {
        if (newCrate == null) {
            return null;
        }
        this.db.addCrateToDb(newCrate);
        if (this.db.getBottleByName(newCrate.getBottle().getName()) == null) {
            this.db.addBottleToDb(newCrate.getBottle());
        } else {
            newCrate.setBottle(this.db.getBottleByName(newCrate.getBottle().getName()));
        }
        return newCrate;
    }

    public Crate updateCrate(final int id, final Crate updatedCrate) {

        final Crate crate = this.db.getCrate(id);

        if (crate == null || updatedCrate == null) {
            return null;
        }

        if (updatedCrate.getPrice() != 0) crate.setPrice(updatedCrate.getPrice());
        if (updatedCrate.getInStock() != 0) crate.setInStock(updatedCrate.getInStock());
        if (updatedCrate.getNoOfBottles() != 0) crate.setNoOfBottles(updatedCrate.getNoOfBottles());

        return crate;
    }

    // ORDER
    public Order getOrder(final int id) {
        return this.db.getOrder(id);
    }

    public boolean deleteOrder(final int id) {
        return this.db.deleteOrder(id);
    }

    public void addOrder(Beverage beverage, final int quantity, final double price) {
        List<OrderItem> orderItems = new ArrayList<>();
        OrderItem orderItem = new OrderItem(10, beverage, quantity);
        decreaseQuantitiesInBeverage(beverage, quantity);
        orderItems.add(orderItem);
        OrderStatus status = OrderStatus.SUBMITTED;
        this.db.addOrderToDb(orderItems,price,status);
    }

    public boolean addBeverageToOrder(final int orderId, Beverage beverage, final int quantity) {
        Order order = this.db.getOrder(orderId);
        // Size  = ID + 1 start count at zero
        OrderItem orderItem = new OrderItem(order.getPositions().size() * 10, beverage, quantity);
        decreaseQuantitiesInBeverage(beverage, quantity);

        return order.getPositions().add(orderItem);
    }

    public boolean deleteBeverageFromOrder(final int orderId, Beverage beverage, final int quantity) {

        Order order = this.db.getOrder(orderId);
        List<OrderItem> orderItems = order.getPositions();
        OrderItem editedOrderItem = null;
        int number = 0;
        boolean edited = true;

        for (OrderItem orderItem : orderItems) {

            if (beverage.equals(orderItem.getBeverage())) {
                number = orderItem.getNumber();
                if (orderItem.getQuantity() > 0) {
                    orderItem.setQuantity(orderItem.getQuantity() - quantity);
                    if (orderItem.getQuantity() == 0) {
                        edited = order.getPositions().remove(orderItem);
                    }
                }
                break;
            } else {
                edited = false;
            }
        }
        increaseQuantitiesInBeverage(beverage, quantity);

        return edited;
    }

    public boolean processOrder(final int orderId) {
        boolean processed = this.db.processOrderCompletion(orderId);
        return processed;
    }

    public List<Order> getAllOrders() {
        return this.db.getAllOrders();
    }

    // ORDERITEM
    public boolean deleteOrderItem(int id, int number) {
        Order order = this.db.getOrder(id);
        OrderItem orderItem = this.db.getOrderItem(id, number);
        return order.getPositions().remove(orderItem);
    }

    public OrderItem getOrderItem(final int id, final int number) {
        return this.db.getOrderItem(id, number);
    }
}
