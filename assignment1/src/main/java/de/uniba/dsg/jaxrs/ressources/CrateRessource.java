package de.uniba.dsg.jaxrs.ressources;

import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import de.uniba.dsg.jaxrs.PaginationHelper;
import de.uniba.dsg.jaxrs.controller.BeverageService;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.ErrorMessage;
import de.uniba.dsg.jaxrs.model.ErrorType;
import de.uniba.dsg.jaxrs.model.api.PaginatedBeverages;
import de.uniba.dsg.jaxrs.model.dto.BeverageDTO;
import de.uniba.dsg.jaxrs.model.dto.CrateDTO;

@Path("beverages/crates")
public class CrateRessource {

    private static final Logger logger = Logger.getLogger("CrateRessource");

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCrates(@Context final UriInfo info,
                            @QueryParam("pageLimit") @DefaultValue("10") final int pageLimit,
                            @QueryParam("page") @DefaultValue("1") final int page) {
        logger.info("Get all Crates. Pagination parameter: page-" + page + " pageLimit-" + pageLimit);

        // Parameter validation
        if (pageLimit < 1 || page < 1) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.INVALID_PARAMETER, "PageLimit or page is less than 1. Read the documentation for a proper handling!");
            return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
        }

        final PaginationHelper<Crate> helper = new PaginationHelper<>(BeverageService.instance.getAllCrates());
        final PaginatedBeverages response = new PaginatedBeverages(helper.getPagination(info, page, pageLimit),
                BeverageDTO.marshallCrates(helper.getPaginatedList(), info.getBaseUri()), info.getRequestUri());

        if (helper.getPaginatedList().isEmpty()) {
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Crates found.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        return Response.ok(response).build();
    }

    @GET
    @Path("{crateId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getCrate(@Context final UriInfo info,@PathParam("crateId") final int id) {
        logger.info("Get Crate with id " + id);
        final Crate crate = BeverageService.instance.getCrate(id);

        if (crate == null) {
            logger.info("No Crate found for id: " + id + ".");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Crate found for id: " + id + ".");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }
        return Response.ok(new BeverageDTO(crate, info.getBaseUri())).build();
    }

    @POST
    @Path("/employee")
    public Response createCrate(final CrateDTO crateDTO, @Context final UriInfo uriInfo) {
        logger.info("Create new Crate.");
        if (crateDTO == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage(ErrorType.INVALID_PARAMETER, "Body was empty")).build();
        }
        final Crate crate = crateDTO.unmarshall();

        BeverageService.instance.addCrate(crate);

        logger.info("Crate with id " + crate.getId() + " successfully created.");
        return Response.created(
                UriBuilder.fromUri(uriInfo.getBaseUri()).path(CrateRessource.class).path(CrateRessource.class, "getCrate").build(crate.getId())).build();
    }

    @PUT
    @Path("employee/{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCrate(@Context final UriInfo uriInfo, @PathParam("crateId") final int id, final CrateDTO updatetCrate) {
        logger.info("Update crate with id " + id + ".");
        if (updatetCrate == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage(ErrorType.INVALID_PARAMETER, "Body was empty")).build();
        }

        final Crate crate = BeverageService.instance.getCrate(id);

        if (crate == null) {
            logger.info("No Crate found for id: " + id + ". Crate cannot be updated.");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Crate found for id: " + id + ". No update could be performed.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        }

        final Crate resultCrate = BeverageService.instance.updateCrate(id, updatetCrate.unmarshall());

        logger.info("Crate with id " + resultCrate.getId() + " successfully updated.");
        return Response.ok().entity(new BeverageDTO(resultCrate, uriInfo.getBaseUri())).build();
    }

    @DELETE
    @Path("employee/{crateId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSpecificCrate(@PathParam("crateId") final int id) {
        logger.info("Delete Crate with id " + id);
        final boolean deleted = BeverageService.instance.deleteCrate(id);

        if (!deleted) {
            logger.info("No Crate found for id: " + id + ". Crate cannot be deleted.");
            final ErrorMessage errorMessage = new ErrorMessage(ErrorType.NOT_FOUND, "No Crate found for id: " + id + ". Crate cannot be deleted.");
            return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
        } else {
            return Response.ok().build();
        }
    }

}
