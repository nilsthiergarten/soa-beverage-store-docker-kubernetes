package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.*;
import de.uniba.dsg.jaxrs.ressources.BottleRessource;
import de.uniba.dsg.jaxrs.ressources.OrderResource;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "order")
@XmlType(propOrder = { "orderId", "positions", "price", "status", "href" })
public class OrderDTO {

    @XmlElement(required = true)
    private int orderId;
    @XmlElement(required = true)
    private List<OrderItemDTO> positions;
    @XmlElement(required = true)
    private double price;
    @XmlElement(required = true)
    private OrderStatus status;
    private URI href;

    public OrderDTO() {

    }

    public OrderDTO(final Order order, final URI baseUri) {
        this.orderId = order.getOrderId();
        this.price = order.getPrice();
        this.positions = OrderItemDTO.marshall(order.getPositions(), baseUri);
        System.out.println("Postitons: " + this.positions);
        this.status = order.getStatus();
        this.href = UriBuilder.fromUri(baseUri)
                .path(OrderResource.class)
                .path(OrderResource.class, "getOrder")
                .build(this.orderId);
    }

    public static List<OrderDTO> marshall(List<Order> orderList, URI baseUri) {
        final ArrayList<OrderDTO> orders = new ArrayList<>();
        for (final Order order : orderList) {
            orders.add(new OrderDTO((Order) order, baseUri));
        }
       return orders;
    }
}


