package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.ressources.CrateRessource;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.*;
import java.net.URI;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "crate")
@XmlType(propOrder = { "id", "price", "inStock", "bottle", "noOfBottles", "href" })
public class CrateDTO {

    private int id;

    @XmlElement(required = true)
    private double price;
    @XmlElement(required = true)
    private int inStock;
    @XmlElement(required = true)
    private BottleDTO bottle;
    @XmlElement(required = true)
    private int noOfBottles;

    private URI href;

    public CrateDTO() {
    }

    public CrateDTO(final Crate crate, final URI baseUri) {
        this.id = crate.getId();
        this.bottle = new BottleDTO(crate.getBottle());
        this.noOfBottles = crate.getNoOfBottles();
        this.price = crate.getPrice();
        this.inStock = crate.getInStock();
        this.href = UriBuilder.fromUri(baseUri)
                .path(CrateRessource.class)
                .path(CrateRessource.class, "getCrate")
                .build(this.id);
    }

    public Crate unmarshall(){
        return new Crate(this.id,this.bottle.unmarshall(),this.noOfBottles,this.price,this.inStock);
    }


}
