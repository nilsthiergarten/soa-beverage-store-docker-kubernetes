package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.ressources.BottleRessource;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.*;
import java.net.URI;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bottle")
@XmlType(propOrder = { "id", "name", "volume",
        "isAlcoholic", "volumePercent", "price", "supplier", "inStock" ,"href"})
public class BottleDTO {

    private int id;
    @XmlElement(required = true)
    private String name;
    @XmlElement(required = true)
    private double price;
    @XmlElement(required = true)
    private int inStock;
    @XmlElement(required = true)
    private double volume;
    @XmlElement(required = true)
    private boolean isAlcoholic;
    @XmlElement(required = true)
    private double volumePercent;
    @XmlElement(required = true)
    private String supplier;

    private URI href;

    public BottleDTO() {
    }

    public BottleDTO(final Bottle bottle, final URI baseUri) {
        new BottleDTO(bottle);
        this.href = UriBuilder.fromUri(baseUri)
                .path(BottleRessource.class)
                .path(BottleRessource.class, "getBottle")
                .build(this.id);
    }

    public BottleDTO(Bottle bottle) {
        this.id = bottle.getId();
        this.name = bottle.getName();
        this.volume = bottle.getVolume();
        this.isAlcoholic = bottle.isAlcoholic();
        this.volumePercent = bottle.getVolumePercent();
        this.price = bottle.getPrice();
        this.supplier = bottle.getSupplier();
        this.inStock = bottle.getInStock();
    }

    public Bottle unmarshall() {
        return new Bottle(this.id, this.name, this.volume, this.isAlcoholic, this.volumePercent, this.price,
                this.supplier, this.inStock);
    }
}
