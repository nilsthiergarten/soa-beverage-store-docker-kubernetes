package de.uniba.dsg.jaxrs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Set;

import de.uniba.dsg.jaxrs.controller.BeverageService;
import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.ressources.*;

@ApplicationPath("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BeverageServiceApi extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> resources = new HashSet<>();
        resources.add(BeverageRessource.class);
        resources.add(CrateRessource.class);
        resources.add(BottleRessource.class);
        resources.add(OrderResource.class);
        resources.add(SwaggerUI.class);
        return resources;
    }
}
