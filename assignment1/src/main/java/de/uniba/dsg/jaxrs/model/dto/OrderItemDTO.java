package de.uniba.dsg.jaxrs.model.dto;

import de.uniba.dsg.jaxrs.model.Beverage;
import de.uniba.dsg.jaxrs.model.Bottle;
import de.uniba.dsg.jaxrs.model.Crate;
import de.uniba.dsg.jaxrs.model.OrderItem;
import de.uniba.dsg.jaxrs.ressources.OrderResource;
import org.glassfish.jersey.server.Uri;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "order")
@XmlType(propOrder = {"number", "beverage", "quantity"})
public class OrderItemDTO {
    @XmlElement(required = true)
    private int number;
    @XmlElement(required = true)
    private BeverageDTO beverage;
    @XmlElement(required = true)
    private int quantity;
    //private URI href;

    public OrderItemDTO() {

    }

    public OrderItemDTO(final OrderItem orderItem, final URI baseUri) {
        this.beverage = BeverageDTO.marshall(orderItem.getBeverage(), baseUri);
        this.quantity = orderItem.getQuantity();
        this.number = orderItem.getNumber();
        System.out.println("Beverage in OrderItemDto: " + beverage);
        //TODO: href OrderItemDTO
        /*
        this.href = UriBuilder.fromUri(baseUri)
                .path(OrderResource.class)
                .path(OrderResource.class, "getOrderItem")
                .build(this.)

         */
    }

    public static List<OrderItemDTO> marshall(final List<OrderItem> orderItemList, final URI baseURI) {
        final ArrayList<OrderItemDTO> orderItems = new ArrayList<>();
        for (final OrderItem orderItem : orderItemList) {
            orderItems.add(new OrderItemDTO(orderItem, baseURI));
        }
        return orderItems;
    }

//    public OrderItem unmarshall() {
//        Beverage newBeverage = this.beverage.unmarshall();
//        return new OrderItem(this.number, newBeverage, this.quantity);
//    }
}



